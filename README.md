# Middleware Projects Master Repo
## Asterio Fiora, Kael D'Alcamo, Giacomo Vercesi

This is the master repo for all 4 project for the PoliMi course of Middleware Technologies for Distributed System, for the academic year 2020/2021.

In order to retrieve all the content, issue:
```
git submodule update --init --recursive
```

Each folder houses a separate `README.md` with some information on the project and instructions to run a local instance.
Each folder in this directory is accompanied with a PDF file of the same name, which is the Design Document of the project. 